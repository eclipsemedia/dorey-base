#!/usr/bin/env php
<?php
require 'vendor/autoload.php';

use DDG\Console\Core\Application;
use DDG\Console\Command\Install;

$app = new Application('dorey', '1.0.0-alpha', array());
$app->add(new Install(null, $app['twig']));
$app->run();

