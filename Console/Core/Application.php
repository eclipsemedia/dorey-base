<?php
namespace DDG\Console\Core;

use Pimple;
use PDO;
use Twig_Loader_Filesystem;
use Twig_Environment;
use Symfony\Component\Console\Application as Console;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Config\ConfigCache;

class Application extends Pimple
{
    /**
     * 
     * @param string $name name of the application
     * @param int $version version of the application
     * @param array $values settings for the application
     */
    public function __construct($name, $version, array $values = array())
    {
        parent::__construct(array('name' => $name, 'version' => $version) + $values + $this->getDefaults());
        $this->getDrupalSettings();
        
        $this['cache'] = function($pimple){
            new ConfigCache($pimple['cache.dir'], $pimple['debug']);
        };
        
        $this['yaml'] = function($pimple){
            return new Parser();
        };
        
        $this['twig.loader'] = function($pimple){
            return new Twig_Loader_Filesystem($pimple['tiwg.views.path']);
        };
        
        $this['twig'] = function($pimple){
            return new Twig_Environment($pimple['twig.loader'], $pimple['twig.options']);
        };
        
        $this['dbal'] = function($pimple){
            // right now I am only supporting the default default database
            if(
                !isset($pimple['databases']) 
                || !isset($pimple['databases']['default']) 
                || !isset($pimple['databases']['default']['default'])
            ){
                return;
            }
            $default = $pimple['databases']['default']['default'];
            switch($default['driver']){
                case 'mysql':
                    if(!$default['port']){
                        $dsn = sprintf('mysql:host=%s;dbname=%s', $default['host'], $default['database']);
                    } 
                    else {
                        $dsn = sprintf('mysql:host=%s;port=%sdbname=%s', $default['host'], $default['port'], $default['database']);
                    }
                    return new PDO($dsn, $default['username'], $default['password']);
            }
        };
        
        $this['console'] = function($pimple){
            $console = new Console($pimple['name'], $pimple['version']);
            return $console;
        };
    }
    
    /**
     * default settings for the application
     * 
     * @return array
     */
    private function getDefaults()
    {
        return array(
            'tiwg.views.path' => 'Console/Views',
            'twig.options' => array(
                'debug' => true,
                'charset' => 'utf-8',
                'cache' => false,
                'auto_reload' => true,
                'autoescape' => false,
                'strict_variables' => true,
                'optimazations' => -1
            ),
            'cache.dir' => __DIR__ . DIRECTORY_SEPARATOR . '../Cache',
            'cace.files' => '',
            'debug' => false
        );
    }
    
    /**
     * loads the settings file and grabs drupal settings
     * so much for no side effects :'(
     * 
     * @return boolean return false if it could not find the file. Otherwise it sets the databases key and returns true
     */
    private function getDrupalSettings()
    {
        $file = __DIR__ . DIRECTORY_SEPARATOR .'../../sites/default/settings.php';
        if(!file_exists($file)){
            return false;
        }
        require $file;
        $this['databases'] = $databases;
        return true;
    }
    
    /**
     * add a command
     * 
     * @param \Symfony\Component\Console\Command\Command $command
     * 
     * @return type
     */
    public function add(Command $command)
    {
        return $this['console']->add($command);
    }
    
    /**
     * add an array of commands
     * 
     * @param array $commands
     * 
     * @return type
     */
    public function addCommands(array $commands)
    {
        return $this['console']->addCommands($commands);
    }
    
    /**
     * render a twig template and get the results
     * 
     * @param string $template template name
     * @param array $values values to pass to the template
     * 
     * @return type
     */
    public function render($template, array $values = array())
    {
        return $this['twig']->render($template, $values);
    }
    
    /**
     * run the console
     */
    public function run()
    {
        $this['console']->run();
    }
    
    /**
     * 
     * @param string  $file                   A YAML file
     * @param bool    $exceptionOnInvalidType true if an exception must be thrown on invalid types (a PHP resource or object), false otherwise
     * @param bool    $objectSupport          true if object support is enabled, false otherwise
     *
     * @return mixed  A PHP value
     *
     * @throws ParseException If the YAML is not valid
     */
    public function parse($file, $exceptionOnInvalidType = false, $objectSupport = false)
    {
        if(!file_exists($file)){
            throw new Exception(sprintf('Could not locate the file %s', $file));
        }
        return $this['yaml']->parse(file_get_contents($file), $exceptionOnInvalidType = false, $objectSupport = false);
    }
}
