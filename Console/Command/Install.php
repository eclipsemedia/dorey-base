<?php
namespace DDG\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use \PDO;
use \PDOException;
use \Twig_Environment;

/**
 * @todo check the password strenght and suggest stronger passwords
 * @todo check if database or user already exist before creating it
 */
class Install extends Command
{
    private $Input;
    private $Output;
    private $Helper;
    private $Twig;
    
    public function __construct($name, Twig_Environment $twig) {
        parent::__construct($name);
        $this->Twig = $twig;
    }
    
    protected function configure()
    {
        $this->setName('install')
            ->setDescription('Create settings.php and install the site');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {        
        // set the properties
        $this->Input = $input;
        $this->Output = $output;
        $this->Helper = $this->getHelperSet()->get('question');
        // ask the basics
        $this->introduction();
        $host = $this->askHostName();
        $dbname = $this->askDatabaseName();
        $conn = $this->buildConnectionString($host, $dbname);
        $user = $this->askUser($dbname);
        $password = $this->askPassword();
        
        // try connecting. If cant connect get root info and create the user/database
        try {
            $drupal_dbal = new PDO($conn, $user, $password);
        } catch (PDOException $e) {
            if(!$this->askCreate()){
                $this->Output->writeln('<error>Can not continue without a proper database connection. Exiting.</error>');
                exit;
            }
            $root_password = $this->askRootPassword($this->Input, $this->Output, $this->Helper);
            $root_conn = sprintf('mysql:host=%s;dbname=', $host);
            try{
                $root_dbal = new PDO($root_conn, 'root', $root_password);
                $this->createDatabaseAndUser($root_dbal, $host, $dbname, $user, $password);
            } catch (Exception $ex) {
                $output->writeln('<error>Failed to create th database. Displaying the error</error>');
                $output->writeln($e->getMessage());
                exit;
            }
        }
        // try connecting again. if it still fails then display the error and exit
        try{
            $drupal_dbal = new PDO($conn, $user, $password);
        } catch(PDOException $e){
            $output->writeln('<error>failed to connect. Displaying the error</error>');
            $output->writeln($e->getMessage());
            exit;
        }
        $output->writeln('succesffully connected with the new database/user');
        
        $file = $this->askFile();
        $this->importFile($user, $password, $host, $dbname, $file);
        $this->buildSettings($dbname, $user, $password, $host);
        return;
    }
    
    private function generatePassword()
    {
        $allowed = str_shuffle('_-!#%+23456789:=?_-!#%+23456789:=?@ABCDEFGHJKMNPRSTUVWXYZabcdefghjkmnpqrstuvwxyz');
        $length = strlen($allowed) - 1;
        $password = array();
        for($i = 0; $i < 16; $i++){
            $key = mt_rand(0, $length);
            $password[] = $allowed[$key];
        }
        return implode('', $password);
    }
    
    private function introduction()
    {
        return $this->Output->write(array(
            'Welcome to the dorey installer',
            '',
            'This will install a preconfigured drupal site by asking a few questions',
            'Make sure you have the mysql root user handy',
            'if you need this to create the database for you',
            '',
            ''
        ), true);
    }
    
    private function askHostName()
    {
        $host = $this->Helper->ask($this->Input, $this->Output, new Question('What is the host name (defaults to 127.0.0.1): ', '127.0.0.1'));
        if(!$host || !preg_match('`([[:alnum:]_\.]+)`', $host)){
            $this->Output->writeln('<error>Invalid host name.</error>');
            exit;
        }
        return $host;
    }
    
    private function askDatabaseName()
    {
        $dbname = $this->Helper->ask($this->Input, $this->Output, new Question('What is the database name: ', null));
        if(!$dbname || !preg_match('`([[:alnum:]_]+)`', $dbname)){
            $this->Output->writeln('<error>Invalid database name.</error>');
            exit;
        }
        return $dbname;
    }
    
    private function buildConnectionString($host, $dbname)
    {
        return sprintf('mysql:host=%s;dbname=%s;charset=utf8', $host, $dbname);
    }
    
    private function askUser($dbname)
    {
        $user = $this->Helper->ask($this->Input, $this->Output, new Question('What is the user name (defaults to the database name): ', $dbname));
        if(!preg_match('`([[:alnum:]_]+)`', $user)){
            $this->Output->writeln('<error>Invalid User name.</error>');
            exit;
        }
        return $user;
    }
    
    private function askPassword()
    {
        $question = new Question('What is the user password (defaults to a randomly genrated password): ', $this->generatePassword());
        $question->setHidden(true);
        $question->setHiddenFallback(false);
        $password = $this->Helper->ask($this->Input, $this->Output, $question);
        if(!$password){
            $this->Output->writeln('<error>Invalid password.</error>');
            exit;
        }
        return $password;
    }
    
    private function askRootPassword()
    {
        $question = new Question('What is the root password: ', null);
        $question->setHidden(true);
        $question->setHiddenFallback(false);
        $password = $this->Helper->ask($this->Input, $this->Output, $question);
        return ($password ?: null);
    }
    
    private function askCreate()
    {
        $this->Output->write(array(
            'Can not connect to the database. Invalid credentials or it doesnt exists.',
            ''
        ), true);
        return $this->Helper->ask($this->Input, $this->Output, new ConfirmationQuestion('Create the database and user (y/n)? <comment>This will replace the database/user of they exists.</comment>', true));
    }
    
    private function createDatabaseAndUser($dbal, $host, $dbname, $user, $password)
    {   
        $dbal->query(sprintf('CREATE DATABASE %s', $dbname));
        $this->Output->writeln('The database has been made');
        $dbal->query(sprintf("CREATE USER %s@%s IDENTIFIED BY '%s'", $user, $host, $password));
        $this->Output->writeln('created the user');
        $dbal->query(sprintf("GRANT ALL ON %s.* TO %s@%s", $dbname, $user, $host));
        $dbal->query('FLUSH PRIVILEGES');
        $this->Output->writeln('Granted the user permissions for the database');
        $this->Output->writeln('The privaliges have been flushed.');
    }
    
    private function askFile()
    {
        $this->Output->writeln(getcwd());
        $file = $this->Helper->ask($this->Input, $this->Output, new Question('Location of file to import (defaults to dump.sql): ', 'dump.sql'));
        if(!file_exists($file)){
            $this->Output->writeln("<error>Can not locate the file. $file</error>");
            exit;
        }
        return $file;
    }
    
    private function buildSettings($database, $user, $password, $host)
    {
        $this->Output->write('creating the settings.php file');
        $settings = $this->Twig->render('settings.twig', array(
            'database' => $database,
            'user' => $user,
            'password' => $password,
            'host' => $host
        ));
        file_put_contents('sites/default/settings.php', $settings);
        $this->Output->writeln('The settings.php file has been saved');
    }
    
    private function importFile($user, $password, $host, $dbname, $file)
    {
        $this->Output->writeln('Importing the database. This can take some time if the database is large.');
        $command = sprintf('mysql -u %s -p%s -h %s %s < %s', $user, $password, $host, $dbname, $file);
        $result = shell_exec($command);
        $this->Output->writeln('The database has been imported');
        return $result;
    }
}