<?php
namespace DDG\Console\Command\User;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use \PDO;
use \PDOException;

class Info extends Command
{
    protected function configure()
    {
        $this->setName('user:info')
            ->setDescription('Get information about a user')
            ->addArgument(
                'names',
                InputArgument::IS_ARRAY | InputArgument::REQUIRED,
                'What is the username or email of the account you would like info on?'
            )->addArgument(
                'email',
                InputArgument::OPTIONAL,
                'Search emails',
                true
            )->addArgument(
                'username',
                InputArgument::OPTIONAL,
                'Search usernames',
                true
            )->addArgument(
                'uid',
                InputArgument::OPTIONAL,
                'Search uid',
                true
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    { 
        $names = $input->getArgument('names');
        $email = $input->getArgument('email');
        $username = $input->getArgument('username');
        if(!$email && !$username){
            $output->writeln('<error>You must enable searching by email or username.</error>');
            exit;
    }
    
        // get connection
        $PDO = new \PDO('mysqli;host=localhost;dbname=', 'root', NULL);
}
    
}
